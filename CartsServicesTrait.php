<?php

namespace common\services\traits;

use common\exceptions\SomeException; // This is example name

// Сейчас бы реализовал не в трейте, а отдельным классом-валидатором, вынеся тексты ошибок в константы, чтобы не хардкодить

trait CartsServicesTrait
{
    /** @var int */
    private $productId;

    /** @var int */
    private $userId;

    public function setProductId(int $id): void
    {
        $this->productId = $id;
    }

    public function setUserId(int $id): void
    {
        $this->userId = $id;
    }

    /**
     * @throws SomeException
     */
    protected function validateProductId(): void
    {
        if (is_null($this->productId)) {
            throw new SomeException('Товар не найден');
        }
    }

    /**
     * @throws NotFoundHttpException
     */
    protected function validateUserId(): void
    {
        if (is_null($this->userId)) {
            throw new SomeException('Пользователь не найден');
        }
    }
}
