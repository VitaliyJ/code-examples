<?php

namespace common\models;

use common\repository\DbRepository;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "templates_cart".
 *
 * @property int $id
 * @property int $user_id
 * @property int $product_id
 * @property int $quantity
 * @property double $amount
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Products $product
 * @property Users $user
 */
class TemplatesCart extends DbRepository
{
    public static function tableName(): string
    {
        return DB_PROVIDERS . '.templates_cart';
    }

    public function rules(): array
    {
        return [
            [['user_id', 'product_id'], 'required'],
            [['user_id', 'product_id', 'quantity', 'created_at', 'updated_at'], 'integer'],
            [['amount'], 'number'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::class, 'targetAttribute' => ['product_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'product_id' => 'Product ID',
            'quantity' => 'Quantity',
            'amount' => 'Amount',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getProduct(): ActiveQuery
    {
        return $this->hasOne(Products::class, ['id' => 'product_id']);
    }

    public function getUser(): ActiveQuery
    {
        return $this->hasOne(Users::class, ['id' => 'user_id']);
    }

    public static function getCartProducts(int $userId, array $selectList = ['product_id', 'quantity']): array
    {
        return static::getDefaultQuery($selectList, 'product_id')
            ->where(['user_id' => $userId])
            ->all();
    }
}
