<?php

namespace common\services\carts;

use common\models\TemplatesCart;
use common\services\Service;
use common\services\traits\CartsServicesTrait;
use common\exceptions\SomeException; // This is example name

/**
 * Class DeleteFromCartService
 * @package common\services\carts
 */
class DeleteFromCartService extends Service
{
    use CartsServicesTrait;

    /**
     * @return bool
     * @throws SomeException
     */
    public function run(): bool
    {
        $this->validateProductId();
        $this->validateUserId();

        return $this->delete();
    }

    private function delete(): bool
    {
        $deletedRows = TemplatesCart::deleteAll([
            'product_id' => $this->productId,
            'user_id' => $this->userId,
        ]);

        return (bool)$deletedRows;
    }
}
