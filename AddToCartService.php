<?php

namespace common\services\carts;

use common\models\TemplatesCart;
use common\services\Service;
use common\services\traits\CartsServicesTrait;
use common\exceptions\SomeException; // This is example name

/**
 * Class AddToCartService
 * @package common\services\carts
 */
class AddToCartService extends Service
{
    use CartsServicesTrait;

    public function setProductId(int $id): void
    {
        $this->productId = $id;
    }

    /**
     * @return bool
     * @throws SomeException
     */
    public function run(): bool
    {
        $this->validateProductId();
        $this->validateUserId();

        if ($this->checkProductAlreadyAdded()) {
            return $this->deleteExistItem();
        }

        return $this->addNewItem();
    }

    protected function checkProductAlreadyAdded(): bool
    {
        $cartItem = TemplatesCart::findOne([
            'user_id' => $this->userId,
            'product_id' => $this->productId,
        ]);

        if (!is_null($cartItem)) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     * @throws SomeException
     */
    private function deleteExistItem(): bool
    {
        $deletingService = new DeleteFromCartService();
        $deletingService->setProductId($this->productId);
        $deletingService->setUserId($this->userId);

        return $deletingService->run();
    }

    private function addNewItem(): bool
    {
        $ci = new TemplatesCart();
        $ci->user_id = $this->userId;
        $ci->product_id = $this->productId;

        return $ci->save();
    }
}
